/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornos;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author nacho
 */
@RunWith(Parameterized.class)
public class EntornosEsLaborableTest {
    private int dia;
    private boolean resultado;

    public EntornosEsLaborableTest(int dia, boolean resultado) {
        this.dia = dia;
        this.resultado = resultado;
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> numeros() {
        return Arrays.asList(new Object[][]{
            {2, true},
            {6, false},
            {7, false},
            {1, true}
        });
    }        

    /**
     * Test of esLaborable method, of class Entornos.
     */
    @Test
    public void testEsLaborable() {
        assertEquals(resultado, Entornos.esLaborable(dia));
    }

   
}
