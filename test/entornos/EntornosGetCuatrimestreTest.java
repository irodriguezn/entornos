/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornos;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author nacho
 */
@RunWith(Parameterized.class)
public class EntornosGetCuatrimestreTest {
    private int mes;
    private int resultado;

    public EntornosGetCuatrimestreTest(int mes, int resultado) {
        this.mes = mes;
        this.resultado = resultado;
    }
    
    @Parameterized.Parameters
    public static Collection<Object[]> numeros() {
        return Arrays.asList(new Object[][]{
            {1, 1},
            {2, 1},
            {5, 2},
            {8, 2},
            {9, 3},
            {11, 3},
            {12, 3}
        });
    }        

   
    /**
     * Test of getCuatrimestre method, of class Entornos.
     */
    @Test
    public void testGetCuatrimestre() {
        assertEquals(resultado, Entornos.getCuatrimestre(mes));
    }
    
}
