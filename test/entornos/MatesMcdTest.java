/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornos;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author nacho
 */
@RunWith(Parameterized.class)
public class MatesMcdTest {
    private int op1;
    private int op2;
    private int resultado;

    
    public MatesMcdTest(int op1, int op2, int resultado) {
        this.op1 = op1;
        this.op2 = op2;
        this.resultado = resultado;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> numeros() {
        return Arrays.asList(new Object[][]{
            {6, 2, 2},
            {6, 3, 3},
            {7, 2, 1},
            {9, 6, 3}
        });
    }    

  
    /**
     * Test of mcd method, of class Mates.
     */
    @Test
    public void testMcd() {
        assertEquals(resultado,new Mates(op1,op2).mcd());
    }

   
}
