/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornos;

import java.util.Arrays;
import java.util.Collection;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

/**
 *
 * @author nacho
 */
@RunWith(Parameterized.class)
public class MatesAbsTest {
    private int op1;
    private int resultado;

    
    public MatesAbsTest(int op1, int resultado) {
        this.op1 = op1;
        this.resultado = resultado;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> numeros() {
        return Arrays.asList(new Object[][]{
            {-3, 3},
            {-5, 5},
            {5, 5},
            {3, 3}
        });
    }    

    
    @Test
    public void testAbs() {
        assertEquals(resultado,new Mates(0,0).abs(op1));
    }
    
}
