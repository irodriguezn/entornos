package entornos;

/**
 *
 * @author lliurex
 * <h1>ENTORNOS BY NACHOROD</h1>
 */
public class Entornos {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Mates m=new Mates(10, 4);
        System.out.println("absoluto de -5="+m.abs(-5));
        System.out.println("MCD de 10 y 4="+m.mcd());
        System.out.println("10 div 4="+m.div());
        System.out.println("10 mod 4="+m.resto());
    }
    
    /**
     * 
     * @param dia  (<b>Dia</b> de la semana 1-lunes, 2-martes, ..., 7-domingo)
     * @return (true o false según sea o no laborable)
     * 
     */
    
    public static boolean esLaborable(int dia) {
        boolean esLaborable=false;
        if (dia>=1 && dia<6) {
            esLaborable=true;
        }
        return esLaborable;
    }
    
    /**
     * Devuelve el cuatrimestre al que corresponde el mes pasado como parámetro
     * @param mes
     * @return 
     */
    public static int getCuatrimestre(int mes) {
        int cuatrim=0;
        if (mes>=1 && mes<=4) {
            cuatrim=1;
        } else if (mes>=5 && mes<=8) {
            cuatrim=2;
        } else if (mes>=9 && mes<=12) {
            cuatrim=3;
        }
        return cuatrim;
    }
    
}


            
