/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package entornos;

/**
 *
 * @author nachorod - Una máquina programando
 */
public class Mates {
    private int op1;
    private int op2;
    
    /**
     * 
     * @param op1 - Primer operando (int)
     * @param op2 - Segundo operando (int)
     */
    public Mates(int op1, int op2) {
        this.op1=op1;
        this.op2=op2;
    }
    
    public int resto() {
        return op1%op2;
    }
    
    public int div() {
        if (op2==0) {
            return Integer.MAX_VALUE;
        } else {
            return op1/op2;
        }
    }
    
    public int mcd() {
        int resto=0;
        int mayor, menor, mcd=1;
        boolean fin=false;
        if (op1>=op2) {
            mayor=op1;
            menor=op2;
        } else {
            mayor=op2;
            menor=op1;
        }
        while (!fin) {
            resto=mayor%menor;
            if (resto==0) {
                mcd=menor;
                break;
            }
            mayor=menor;
            menor=resto;
        }
        return mcd;
    }
    
    public int abs(int num){
        int abs;
        if (num>0) {
            abs=num;
        } else {
            abs=-num;
        }
        return abs;
    }
    
}
